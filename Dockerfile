# Use the official Python image as a base image
FROM python:3.9-slim

ENV FLASK_APP=counter-service.py

# Set the working directory in the container
WORKDIR /app

# Copy the dependencies file to the working directory
COPY ./webapp/requirements.txt /app/

# Install any dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the application code to the working directory
COPY ./webapp /app/

# Expose the port on which the Flask app will run
EXPOSE 80

# Run the Flask application
CMD ["flask", "run", "--host=0.0.0.0", "--port=80"]
